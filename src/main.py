
import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn import model_selection
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_squared_log_error, r2_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, Ridge, LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import AdaBoostRegressor
import pathlib

TEST_PERCENTAGE = 0.3


def main():
	
	# read the data
	filepath = pathlib.Path(__file__).parent.parent / "data" / "train.csv"
	main_dataframe = pd.read_csv(filepath)
	#print(main_dataframe.head())	   # hooray and a cheer - it works
	
	# preprocess the data
	main_dataframe = preprocess(main_dataframe)
	
	# split the data
	train_dataframe, test_dataframe, train_class_data, test_class_data = split_data(main_dataframe, "count", TEST_PERCENTAGE)
	
	
	# create a list of regressors to test
	regressors = [LinearRegression(), Ridge(), Ridge(alpha=0.8), Ridge(alpha=1.2), Ridge(alpha=1.4), #LogisticRegression(),
				  KNeighborsRegressor(), KNeighborsRegressor(metric="chebyshev"), KNeighborsRegressor(n_neighbors=3),	#KNeighborsRegressor(metric="manhattan"), KNeighborsRegressor(metric="euclidean"),
				  DecisionTreeRegressor(),
				  RandomForestRegressor(n_estimators=10), RandomForestRegressor(n_estimators=30), RandomForestRegressor(n_estimators=100)]
	
	for regressor in regressors:
		print(type(regressor))
		regressor.fit(train_dataframe, train_class_data)
		train_and_test(regressor, train_dataframe, train_class_data, test_dataframe, test_class_data)


def preprocess(dataframe):
	preprocessed_dataframe = dataframe
	preprocessed_dataframe.rename(columns={"weathersit": "weather",
							   "mnth": "month",
							   "hr": "hour",
							   "yr": "year",
							   "hum": "humidity",
							   "cnt": "count"},
					 inplace=True)
	preprocessed_dataframe = one_hot_encode_multicolumn(preprocessed_dataframe, ["season", "weekday", "month", "weather"])		# add "hour"?
	return preprocessed_dataframe


def split_data(dataframe, class_name, test_size=0.3):
	attributes_to_use = dataframe[dataframe.columns]		# by default use all the attributes
	
	# exclude the following
	del attributes_to_use["casual"]
	del attributes_to_use["registered"]
	del attributes_to_use[class_name]
	
	class_attribute = dataframe[class_name]		# the class attribute
	return train_test_split(attributes_to_use, class_attribute, test_size=test_size, random_state=42)


def one_hot_encode_multicolumn(dataframe, column_names):
	one_hot_encoded_dataframe = dataframe
	for column_name in column_names:
		one_hot_encoded_dataframe = one_hot_encode_column(one_hot_encoded_dataframe, column_name)
	
	return one_hot_encoded_dataframe


def one_hot_encode_column(dataframe, column_name):
	encoder = OneHotEncoder(categories='auto')
	encoded_columns = encoder.fit_transform(dataframe[column_name].values.reshape(-1, 1)).toarray()
	encoded_dataframe_part = pd.DataFrame(encoded_columns, columns=[column_name + "_" + str(int(i)) for i in range(encoded_columns.shape[1])])
	
	new_dataframe = pd.concat([dataframe, encoded_dataframe_part], axis=1)
	del new_dataframe[column_name]
	return new_dataframe


def train_and_test(regressor, train_dataframe, train_class_data, test_dataframe, test_class_data):
	# train the given regressor
	regressor.fit(train_dataframe, train_class_data)
	
	# use the regressor to make predictions and then evaluate them
	predictions = regressor.predict(test_dataframe)
	print_errors(predictions, test_class_data)
	
	# if the regressor is not already an ensemble regressor or a KNeighborRegressor, try it with AdaBoost
	if type(regressor) != RandomForestRegressor and type(regressor) != KNeighborsRegressor and type(regressor) != AdaBoostRegressor:
		print("With boosting (50 instances):")
		boosted_regressor = AdaBoostRegressor(base_estimator=regressor, n_estimators=50)
		train_and_test(boosted_regressor, train_dataframe, train_class_data, test_dataframe, test_class_data)
		
	print()


def print_errors(predictions, test_class_data):
	# replace negative values in predictions with zero - just to be able to use RMSLE
	for i, y in enumerate(predictions):
		if predictions[i] < 0:
			predictions[i] = 0
	
	# print the RMSLE and R2 of the predictions in comparison with the test data
	print('RMSLE:', np.sqrt(mean_squared_log_error(test_class_data, predictions)))
	print('R2:', r2_score(test_class_data, predictions))



main()
